<?php


namespace App;


class GeomFigure
{
    private $name;
    private $numberQutochk;

    /**
     * GeomFigure constructor.
     * @param $name
     * @param $numberQutochk
     */
    public function __construct($name, $numberQutochk)
    {
        $this->name = $name;
        $this->numberQutochk = $numberQutochk;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getNumberQutochk()
    {
        return $this->numberQutochk;
    }

    /**
     * @param mixed $numberQutochk
     */
    public function setNumberQutochk($numberQutochk)
    {
        $this->numberQutochk = $numberQutochk;
    }


}