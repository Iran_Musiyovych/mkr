<?php


namespace App\GeomFigure;


class Figure
{
    public function Q() {
        $figures = array();
        $figures[] = new GeomFigure("Circle", 0);
        $figures[] = new GeomFigure("Square", 4);
        $figures[] = new GeomFigure("Triangle", 3);
        return $figures;
    }

    public function mostNumberQutochkiv(){
        $result = $this->Q()->getNumberQutochk();
        return max ($result);
    }

    public function showFigures(){
        $result = $this->Q()->getNumberQutochk("3");
        return $result;
    }

    public function o(){
        echo $this->showFigures();
        echo $this->mostNumberQutochkiv();
    }
}